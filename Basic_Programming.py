class KeyProgramming:
    def __init__(self):
        self.key_value_store = {}

    def add_key_value(self, key, value):
        """Add a key-value pair to the store."""
        self.key_value_store[key] = value
        print(f"Key '{key}' added with value '{value}'.")

    def get_value(self, key):
        """Retrieve the value associated with a key."""
        if key in self.key_value_store:
            value = self.key_value_store[key]
            print(f"Value for key '{key}': {value}")
        else:
            print(f"Key '{key}' not found.")

    def delete_key(self, key):
        """Delete a key and its associated value from the store."""
        if key in self.key_value_store:
            del self.key_value_store[key]
            print(f"Key '{key}' deleted.")
        else:
            print(f"Key '{key}' not found.")

    def display_all_keys(self):
        """Display all keys and their values in the store."""
        if self.key_value_store:
            print("Key-Value Store:")
            for key, value in self.key_value_store.items():
                print(f"  '{key}': '{value}'")
        else:
            print("Key-Value Store is empty.")

# Example usage:
key_programming = KeyProgramming()

key_programming.add_key_value("name", "John Doe")
key_programming.add_key_value("age", 25)

key_programming.display_all_keys()

key_programming.get_value("name")
key_programming.get_value("city")

key_programming.delete_key("age")

key_programming.display_all_keys()
