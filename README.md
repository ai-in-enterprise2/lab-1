# ToDOListApp


This code defines a TodoList class with methods to perform various tasks on a simple todo list.
Detailed Explanation:



- TodoList Class:
The class is initialized with an empty list (self.tasks) to store tasks.



- add_task Method:
Adds a new task to the todo list. The task is stored as a dictionary with a description and completion status.



- mark_completed Method:
Marks a task as completed based on the provided task index. The completion status is updated in the task dictionary.



- view_tasks Method:
Displays the current list of tasks with their descriptions and completion status. If the list is empty, it notifies that the todo list is empty.



- delete_task Method:
Deletes a task from the list based on the provided task index.
