class TodoList:
    def __init__(self):
        self.tasks = []

    def add_task(self, task):
        """Add a task to the todo list."""
        self.tasks.append({"task": task, "completed": False})
        print(f"Task '{task}' added to the todo list.")

    def mark_completed(self, task_index):
        """Mark a task as completed."""
        if 0 <= task_index < len(self.tasks):
            self.tasks[task_index]["completed"] = True
            print(f"Task '{self.tasks[task_index]['task']}' marked as completed.")
        else:
            print("Invalid task index.")

    def view_tasks(self):
#View the list of tasks.
        if self.tasks:
            print("Todo List:")
            for index, task in enumerate(self.tasks):
                status = "Completed" if task["completed"] else "Pending"
                print(f"  {index + 1}. {task['task']} - {status}")
        else:
            print("Todo List is empty.")

    def delete_task(self, task_index):
        """Delete a task from the todo list."""
        if 0 <= task_index < len(self.tasks):
            deleted_task = self.tasks.pop(task_index)
            print(f"Task '{deleted_task['task']}' deleted from the todo list.")
        else:
            print("Invalid task index.")

# Example usage:
todo_list = TodoList()

todo_list.add_task("Write code for project")
todo_list.add_task("Read a book")

todo_list.view_tasks()

todo_list.mark_completed(1)

todo_list.view_tasks()

todo_list.delete_task(0)

todo_list.view_tasks()

